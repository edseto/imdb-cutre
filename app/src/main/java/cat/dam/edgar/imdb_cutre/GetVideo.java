package cat.dam.edgar.imdb_cutre;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class GetVideo extends AsyncTask<String, String, String>
{
    private final ModelFilm film;
    private final String language;

    public GetVideo(ModelFilm film, String language)
    {
        this.film = film;
        this.language = language;
    }

    @Override
    protected String doInBackground(String... strings)
    {
        String idMovie = film.getId(), actual="";
        String videoUrl = Constants.API_LINK + idMovie + "/videos?api_key="+Constants.API_KEY+"&language="+ language;

        URL url;
        HttpsURLConnection connection = null;

        try {
            url = new URL(videoUrl);
            connection = (HttpsURLConnection) url.openConnection();

            InputStream is = connection.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);

            int data = isr.read();
            while (data != -1){
                actual += (char) data;
                data = isr.read();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        if(connection != null) connection.disconnect();

        return actual;
    }

    @Override
    protected void onPostExecute(String s)
    {
        String video;
        try {
            JSONObject json = new JSONObject(s);
            JSONArray jsonArray = json.getJSONArray("results");

            JSONObject jsonObject = jsonArray.getJSONObject(0);
            if (jsonObject.getString("site").equals("YouTube")){
                video = jsonObject.getString("key");

                film.setVideo(video);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
