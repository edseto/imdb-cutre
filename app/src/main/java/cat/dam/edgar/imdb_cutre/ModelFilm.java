package cat.dam.edgar.imdb_cutre;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class ModelFilm implements Serializable
{
    private String id, title, overview, poster, rating, release, originalTitle, genres, video;

    // Getters
    public String getId() { return id; }
    public String getTitle() { return title; }
    public String getOverview() { return overview; }
    public String getPoster() { return poster; }
    public String getRating() { return rating; }
    public String getRelease() { return release; }
    public String getOriginalTitle() { return originalTitle; }
    public String getGenres() { return genres; }
    public String getVideo() { return video; }

    // Setters
    public void setId(String id) { this.id = id; }
    public void setTitle(String title) { this.title = title; }
    public void setOverview(String overview) { this.overview = overview; }
    public void setPoster(String poster) { this.poster = poster; }
    public void setRating(String rating) { this.rating = rating; }
    public void setRelease(String release) { this.release = release; }
    public void setOriginalTitle(String originalTitle) { this.originalTitle = originalTitle; }
    public void setGenres(String genres) { this.genres = genres; }
    public void setVideo(String video) { this.video = video; }

    // Constructors
    public ModelFilm() {}

    public ModelFilm(String id, String title, String overview, String poster, String rating, String release, String originalTitle, String genres)
    {
        this.id = id;
        this.title = title;
        this.overview = overview;
        this.poster = poster;
        this.rating = rating;
        this.release = release;
        this.originalTitle = originalTitle;
        this.genres = genres;
    }

    public ModelFilm(JSONObject jsonMovie, String language) throws JSONException
    {
        String release;
        GetMovieGenres getGenres;
        GetVideo getVideo;

        setId(jsonMovie.getString("id"));
        setTitle(jsonMovie.getString("title"));
        setOverview(jsonMovie.getString("overview"));
        setPoster(Constants.IMAGE_LINK +jsonMovie.getString("poster_path"));

        setOriginalTitle(jsonMovie.getString("original_title"));
        setRating(jsonMovie.getString("vote_average"));

        release = jsonMovie.getString("release_date");
        if(!release.isEmpty()) release = release.substring(0, 4);
        setRelease(release);

        getGenres = new GetMovieGenres(getId(), this, language);
        getVideo = new GetVideo(this, language);
        getGenres.execute();
        getVideo.execute();
    }
}
