package cat.dam.edgar.imdb_cutre;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;

public class MovieDetail extends AppCompatActivity
{
    private TextView tv_title, tv_original_title, tv_release, tv_rating, tv_overview, tv_genres;
    private ModelFilm film;
    private ImageView iv_detail_poster;
    private RecyclerView rv_cast;
    private ArrayList<ModelActor> cast;
    private FloatingActionButton fab_home, fab_menu, fab_favourite, fab_video;
    private boolean clicked = false;
    private Animation from_bottom, to_bottom;
    private Dialog preLoader, video;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);

        setVariables();
        setListener();
        printData();
        setCast();
    }

    private void setVariables()
    {
        iv_detail_poster = (ImageView) findViewById(R.id.iv_detail_poster);
        rv_cast = (RecyclerView) findViewById(R.id.rv_detail_cast);
        preLoader = new Dialog(this);
        video = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        setTextViews();
        setFilmData();
        setFabVar();
        setAnimationVar();
    }

    private void setTextViews()
    {
        tv_title = (TextView) findViewById(R.id.tv_detail_title);
        tv_original_title = (TextView) findViewById(R.id.tv_detail_original_title);
        tv_rating = (TextView) findViewById(R.id.tv_detail_rating);
        tv_release = (TextView) findViewById(R.id.tv_detail_release);
        tv_genres = (TextView) findViewById(R.id.tv_detail_genres);
        tv_overview = (TextView) findViewById(R.id.tv_detail_overview);
    }

    private void setFabVar()
    {
        fab_home = (FloatingActionButton) findViewById(R.id.fab_movie_detail_home);
        fab_menu = (FloatingActionButton) findViewById(R.id.fab_movie_detail_menu);
        fab_favourite = (FloatingActionButton) findViewById(R.id.fab_movie_detail_favourite);
        fab_video = (FloatingActionButton) findViewById(R.id.fab_movie_detail_video);
    }

    private void setAnimationVar()
    {
        to_bottom = AnimationUtils.loadAnimation(this, R.anim.anim_to_bottom);
        from_bottom = AnimationUtils.loadAnimation(this, R.anim.anim_from_bottom);
    }

    private void setFilmData()
    {
        Bundle filmData = this.getIntent().getExtras();
        film = (ModelFilm) filmData.getSerializable("movie");
    }

    /**
     * Floating Menu Listeners
     */
    private void setListener()
    {
        fab_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goHomePage();
            }
        });

        fab_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onMenuClick();
            }
        });

        fab_favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.createToast(MovieDetail.this, getResources().getString(R.string.add_fav));
                addFavourite();
            }
        });

        fab_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTrailer();
            }
        });
    }

    private void showTrailer()
    {
        video.setContentView(R.layout.video_player);

        YouTubePlayerView yt_trailer = (YouTubePlayerView) video.findViewById(R.id.yt_trailer);
        getLifecycle().addObserver(yt_trailer);
        yt_trailer.getYouTubePlayerWhenReady(youTubePlayer -> {youTubePlayer.cueVideo(film.getVideo(), 0);});

        videoListener(yt_trailer);
        video.show();
    }

    /**
     * Release YoutubePlayer on dismiss Dialog
     * @param yt_trailer YoutubePlayerView to release
     */
    private void videoListener(YouTubePlayerView yt_trailer)
    {
        video.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                yt_trailer.release();
            }
        });
    }

    /**
     * On Menu Click Methods
     */
    private void onMenuClick()
    {
        setVisibility(clicked);
        setAnimation(clicked);
        clicked = !clicked;
    }

    /**
     * Change Menu items visibility
     * @param click Boolean true: open, false: close
     */
    private void setVisibility(Boolean click)
    {
        if(!click){
            fab_home.setVisibility(View.VISIBLE);
            fab_favourite.setVisibility(View.VISIBLE);
            if (film.getVideo()!=null) fab_video.setVisibility(View.VISIBLE);
        } else {
            fab_home.setVisibility(View.INVISIBLE);
            fab_favourite.setVisibility(View.INVISIBLE);
            fab_video.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * Start Menu items Animation
     * @param click Boolean true: open, false: close
     */
    private void setAnimation(Boolean click)
    {
        if(!click){
            fab_home.startAnimation(from_bottom);
            fab_favourite.startAnimation(from_bottom);
            if (film.getVideo()!=null) fab_video.startAnimation(from_bottom);
            fab_menu.setImageResource(R.drawable.ic_close);
        } else {
            fab_home.startAnimation(to_bottom);
            fab_favourite.startAnimation(to_bottom);
            if (film.getVideo()!=null) fab_video.startAnimation(to_bottom);
            fab_menu.setImageResource(R.drawable.ic_menu);
        }
    }

    /**
     * Set Movie Data
     */
    private void printData()
    {
        tv_title.setText(film.getTitle());
        tv_release.setText(film.getRelease());
        tv_rating.setText(film.getRating());
        tv_overview.setText(film.getOverview());
        tv_original_title.setText(film.getOriginalTitle());
        tv_genres.setText(film.getGenres());
        setBackgroundPoster();
    }

    /**
     * Set Movie Poster
     */
    private void setBackgroundPoster()
    {
        Glide.with(getApplicationContext())
                .load(film.getPoster())
                .into(iv_detail_poster);
    }

    /**
     * Add Movie to Favourite List
     */
    private void addFavourite()
    {
        Database db = new Database();
        String email = getUserLogged();

        db.insertFavouriteMovies(email, film);
    }

    /**
     * Get User Logged previous
     * @return String: User logged email
     */
    private String getUserLogged()
    {
        SharedPreferences preferences = getSharedPreferences("LOGIN", 0);
        return preferences.getString("LOGIN", null);
    }

    /**
     * Go HomePage Activity
     */
    private void goHomePage()
    {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    /**
     * Set Actors CardViews
     */
    private void setCast()
    {
        cast = new ArrayList<>();
        GetActors getActors = new GetActors(film.getId());
        getActors.execute();
    }

    /**
     * Print Actors data
     * @param cast ModelActor ArrayList
     */
    private void printCastData(ArrayList<ModelActor> cast)
    {
        CastRecyclerAdapter adapter = new CastRecyclerAdapter(this, cast);
        rv_cast.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rv_cast.setAdapter(adapter);
    }

    /**
     * Get Actors Data
     */
    public class GetActors extends AsyncTask<String, String, String>
    {
        private final String idMovie;

        public GetActors(String movie) { this.idMovie = movie; }

        @Override
        protected void onPreExecute()
        {
            Constants.preLoader(preLoader);
        }

        @Override
        protected String doInBackground(String... strings)
        {
            String actual = "";
            String movieCast = Constants.API_LINK + idMovie + "/credits?api_key="+Constants.API_KEY;

            URL url;
            HttpsURLConnection connection = null;

            try {
                url = new URL(movieCast);
                connection = (HttpsURLConnection) url.openConnection();

                InputStream is = connection.getInputStream();
                InputStreamReader isr = new InputStreamReader(is);

                int data = isr.read();
                while (data != -1){
                    actual += (char) data;
                    data = isr.read();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            if(connection != null) connection.disconnect();

            return actual;
        }

        // When finish doInBackground function, goes here
        @Override
        protected void onPostExecute(String s)
        {
            preLoader.dismiss();
            cast.clear();
            try {
                JSONObject json = new JSONObject(s);
                JSONArray jsonArray = json.getJSONArray("cast");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonActor = jsonArray.getJSONObject(i);
                    if(!jsonActor.getString("profile_path").equals("null")){
                        ModelActor actor = new ModelActor(jsonActor);
                        cast.add(actor);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            printCastData(cast);
        }
    }
}