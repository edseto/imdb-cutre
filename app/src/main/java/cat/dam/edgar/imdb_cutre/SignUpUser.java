package cat.dam.edgar.imdb_cutre;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.security.NoSuchAlgorithmException;

public class SignUpUser extends AppCompatActivity
{
    private final Database db = new Database();
    private final FirebaseFirestore firebase = FirebaseFirestore.getInstance();
    private final CollectionReference usersRef = firebase.collection(Constants.COLLECTION_USERS);
    private Button btn_signUp, btn_logIn;
    private EditText et_username, et_email, et_password, et_confirmPassword;
    private FloatingActionButton fab_home;
    private ModelUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);

        setVariables();
        setListener();
    }

    private void setVariables()
    {
        btn_signUp = (Button) findViewById(R.id.btn_register);
        btn_logIn = (Button) findViewById(R.id.btn_have_account);
        fab_home = (FloatingActionButton) findViewById(R.id.fab_signUp_home);
        setEditTextVar();
    }

    private void setEditTextVar()
    {
        et_username = (EditText) findViewById(R.id.et_username);
        et_email = (EditText) findViewById(R.id.et_email);
        et_password = (EditText) findViewById(R.id.et_password);
        et_confirmPassword = (EditText) findViewById(R.id.et_confirm_password);
    }

    private void setListener()
    {
        btn_signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkFields()) {
                    String email = et_email.getText().toString(), username = et_username.getText().toString();
                    checkEmail(email, username);
                }
            }
        });

        btn_logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goActivity(LogInUser.class);
            }
        });

        fab_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goActivity(MainActivity.class);
            }
        });
    }

    /**
     * Check if fields are empty
     * @return true if correct, false if incorrect
     */
    private boolean checkFields()
    {
        Resources res = getResources();

        if (TextUtils.isEmpty(et_username.getText())){
            et_username.setError(res.getString(R.string.username_error));
            return false;
        }

        if (TextUtils.isEmpty(et_email.getText())){
            et_email.setError(res.getString(R.string.email_error));
            return false;
        }

        return checkPasswords();
    }

    /**
     * Check Passwords fields
     * @return true if not empty and passwords match or false if incorrect
     */
    private boolean checkPasswords()
    {
        Resources res = getResources();
        String password = et_password.getText().toString();
        String confirmPassword = et_confirmPassword.getText().toString();

        if (TextUtils.isEmpty(password)){
            et_password.setError(res.getString(R.string.password_error));
            return false;
        }

        if (TextUtils.isEmpty(confirmPassword)){
            et_confirmPassword.setError(res.getString(R.string.confirm_password_error));
            return false;
        }

        if (!password.equals(confirmPassword)){
            et_confirmPassword.setError(res.getString(R.string.password_no_match));
            return false;
        }

        return true;
    }

    /**
     * Create ModelUser to insert to FireBase
     */
    private void createUserObject()
    {
        user = new ModelUser();

        user.setEmail(et_email.getText().toString());
        user.setUsername(et_username.getText().toString());
        try {
            user.setPassword(et_password.getText().toString(), true);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private void signUp()
    {
        createUserObject();
        db.insertUser(user);
        saveLogIn();
        goProfile();
    }

    /**
     * Save User Log in in preferences
     */
    private void saveLogIn()
    {
        SharedPreferences preferences = getApplicationContext().getSharedPreferences("Preferences", 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("LOGIN", user.getEmail());
        editor.apply();
    }

    /**
     * Check if email already in DataBase
     * @param email String: User email to check if exists
     * @param username String: Username to check if email no exists
     */
    private void checkEmail(String email, String username)
    {
        usersRef.document(email).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>()
        {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task)
            {
                Resources res = getResources();
                if (task.isSuccessful()){
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) Constants.createToast(SignUpUser.this, res.getString(R.string.email_exists));
                    else checkUsername(username);
                } else {
                    Constants.createToast(SignUpUser.this, res.getString(R.string.signUp_error));
                }
            }
        });
    }

    /**
     * Check if username already in DataBase
     * @param username String: Username to check if exists
     */
    private void checkUsername(String username)
    {
        Resources res = getResources();
        Query query = usersRef.whereEqualTo("username", username);

        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){
                    if (!task.getResult().isEmpty()) Constants.createToast(SignUpUser.this, res.getString(R.string.username_exists));
                    else signUp();
                } else {
                    Constants.createToast(SignUpUser.this, res.getString(R.string.signUp_error));
                }
            }
        });
    }

    /**
     * Go to User Profile Activity
     */
    private void goProfile()
    {
        Intent intent = new Intent(this, UserProfile.class);
        Bundle bundle = new Bundle();

        bundle.putSerializable("user", user);
        intent.putExtras(bundle);

        startActivity(intent);
    }

    /**
     * Go to Another Activity
     * @param activity Activity to go (ex: SignUp.class)
     */
    private void goActivity(Class activity)
    {
        Intent intent = new Intent(this, activity);
        startActivity(intent);
    }
}