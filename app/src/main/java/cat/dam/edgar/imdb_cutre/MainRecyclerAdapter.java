package cat.dam.edgar.imdb_cutre;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class MainRecyclerAdapter extends RecyclerView.Adapter<MainRecyclerAdapter.ViewHolder>
{
    private final Context context;
    private final ArrayList<ModelFilm> movieList;

    public MainRecyclerAdapter(Context context, ArrayList<ModelFilm> movieList)
    {
        this.context = context;
        this.movieList = movieList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View v;
        LayoutInflater inflater = LayoutInflater.from(context);
        v = inflater.inflate(R.layout.movie_item, parent, false);
        setCardsListeners(v);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        holder.tv_title.setText(movieList.get(position).getTitle());
        holder.tv_release.setText(movieList.get(position).getRelease());

        Glide.with(context)
                .load(movieList.get(position).getPoster())
                .into(holder.iv_poster);
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    private void setCardsListeners(View v)
    {
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MovieDetail.class);
                Bundle bundle = new Bundle();
                TextView title = (TextView) v.findViewById(R.id.tv_title);
                int filmPosition = searchFilmInArray(title.getText().toString());

                bundle.putSerializable("movie", movieList.get(filmPosition));
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });
    }

    private int searchFilmInArray(String title)
    {
        boolean found = false;
        int index = 0;

        while (!found && (index < movieList.size())){
            if(movieList.get(index).getTitle().equals(title)) found = true;
            else index++;
        }

        return found ? index : -1;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tv_title, tv_release;
        ImageView iv_poster;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
            tv_release = (TextView) itemView.findViewById(R.id.tv_release);
            iv_poster = (ImageView) itemView.findViewById(R.id.iv_poster);
        }
    }
}
