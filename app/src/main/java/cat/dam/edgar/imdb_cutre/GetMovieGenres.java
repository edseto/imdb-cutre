package cat.dam.edgar.imdb_cutre;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class GetMovieGenres extends AsyncTask<String, String, String>
{
    private final String id;
    private final ModelFilm film;
    private final String language;

    public GetMovieGenres(String id, ModelFilm film, String language)
    {
        this.id = id;
        this.film = film;
        this.language = language;
    }
    @Override
    protected String doInBackground(String... strings)
    {
        String actual = "";
        String movieUrl = Constants.API_LINK + id + "?api_key="+Constants.API_KEY+"&language="+language;

        URL url;
        HttpsURLConnection connection = null;

        try {
            url = new URL(movieUrl);
            connection = (HttpsURLConnection) url.openConnection();

            InputStream is = connection.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);

            int data = isr.read();
            while (data != -1){
                actual += (char) data;
                data = isr.read();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        if(connection != null) connection.disconnect();

        return actual;
    }

    @Override
    protected void onPostExecute(String s)
    {
        String genres = "";

        try {
            JSONObject json = new JSONObject(s);
            JSONArray jsonArray = json.getJSONArray("genres");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonGenre = jsonArray.getJSONObject(i);
                genres += jsonGenre.getString("name");
                if (i < jsonArray.length()-1) genres += ", ";
            }

            film.setGenres(genres);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}