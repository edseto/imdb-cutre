package cat.dam.edgar.imdb_cutre;

import android.app.Dialog;
import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Constants
{
    public static final String API_KEY = "90aabc36d28797767b91a3746c822053";
    public static final String API_LINK = "https://api.themoviedb.org/3/movie/";
    public static final String FILTER_LINK = "https://api.themoviedb.org/3/discover/movie";
    public static final String GENRE_LIST_LINK = "https://api.themoviedb.org/3/genre/movie/list";
    public static final String SEARCH_LINK = "https://api.themoviedb.org/3/search/movie";
    public static final String IMAGE_LINK = "https://image.tmdb.org/t/p/w500";
    public static final String COLLECTION_USERS = "users";

    /**
     * Make a Toast Message
     * @param context Context: Application context
     * @param description String: Toast message
     */
    public static void createToast(Context context, String description)
    {
        Toast.makeText(context, description, Toast.LENGTH_SHORT).show();
    }

    /**
     * Encrypt Password
     * @param password String: Password to encrypt
     * @return String: Encrypted password
     * @throws NoSuchAlgorithmException
     */
    public static String encryptPassword(String password) throws NoSuchAlgorithmException
    {
        String encrypted = "";
        byte[]bytes = password.getBytes();

        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(bytes);

        byte[]digest = md.digest();

        for (byte b : digest) {
            String st = String.format("%02X", b);
            encrypted += st;
        }

        return encrypted;
    }

    /**
     * PreLoader
     */
    public static void preLoader(Dialog preLoader)
    {
        preLoader.setContentView(R.layout.pre_loader);
        preLoader.setCanceledOnTouchOutside(false);
        preLoader.setCancelable(false);
        startPreLoaderAnimations(preLoader);
        preLoader.show();
    }

    private static void startPreLoaderAnimations(Dialog preLoader)
    {
        Animation rotate = AnimationUtils.loadAnimation(preLoader.getContext(), R.anim.anim_loading_rotate);

        preLoader.findViewById(R.id.iv_loading).startAnimation(rotate);
    }
}
