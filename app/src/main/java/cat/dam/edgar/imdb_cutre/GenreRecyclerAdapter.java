package cat.dam.edgar.imdb_cutre;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class GenreRecyclerAdapter extends RecyclerView.Adapter<GenreRecyclerAdapter.ViewHolder>
{
    private final Context context;
    private final ArrayList<ModelGenre> genreList;

    public GenreRecyclerAdapter(Context context, ArrayList<ModelGenre> genreList)
    {
        this.context = context;
        this.genreList = genreList;
    }

    @NonNull
    @Override
    public GenreRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View v;
        LayoutInflater inflater = LayoutInflater.from(context);
        v = inflater.inflate(R.layout.genre_item, parent, false);

        return new GenreRecyclerAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull GenreRecyclerAdapter.ViewHolder holder, int position)
    {
        final ModelGenre genreObj = genreList.get(position);

        holder.genre.setText(genreObj.getName());
        holder.genre.setTag(genreObj.getId());

        holder.genre.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                genreObj.setChecked(isChecked);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return genreList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        CheckBox genre;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            genre = (CheckBox) itemView.findViewById(R.id.check_genre);
        }
    }
}
