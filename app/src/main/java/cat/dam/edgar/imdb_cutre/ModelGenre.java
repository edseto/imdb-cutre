package cat.dam.edgar.imdb_cutre;

import org.json.JSONException;
import org.json.JSONObject;

public class ModelGenre
{
    private String id;
    private String name;
    private boolean checked;

    public String getId() { return id; }
    public String getName() { return name; }
    public boolean isChecked() { return checked; }

    public void setId(String id) { this.id = id; }
    public void setName(String name) { this.name = name; }
    public void setChecked(boolean checked) { this.checked = checked; }

    public ModelGenre()
    {}

    public ModelGenre(String id, String name, boolean checked)
    {
        this.id = id;
        this.name = name;
        this.checked = checked;
    }

    public ModelGenre(JSONObject jsonGenre) throws JSONException
    {
        setId(jsonGenre.getString("id"));
        setName(jsonGenre.getString("name"));
    }
}
