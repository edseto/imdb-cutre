package cat.dam.edgar.imdb_cutre;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class CastRecyclerAdapter extends RecyclerView.Adapter<CastRecyclerAdapter.ViewHolder>
{
    private final Context context;
    private final ArrayList<ModelActor> cast;

    public CastRecyclerAdapter(Context context, ArrayList<ModelActor> cast)
    {
        this.context = context;
        this.cast = cast;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        LayoutInflater inflater = LayoutInflater.from(context);
        v = inflater.inflate(R.layout.movie_detail_cast, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tv_actor.setText(cast.get(position).getName());
        holder.tv_character.setText(cast.get(position).getCharacter());

        Glide.with(context)
                .load(cast.get(position).getImage())
                .into(holder.iv_actor);
    }

    @Override
    public int getItemCount() { return cast.size(); }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tv_actor, tv_character;
        ImageView iv_actor;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_actor = (TextView) itemView.findViewById(R.id.tv_actor);
            tv_character = (TextView) itemView.findViewById(R.id.tv_character);
            iv_actor = (ImageView) itemView.findViewById(R.id.iv_cast);
        }
    }
}
