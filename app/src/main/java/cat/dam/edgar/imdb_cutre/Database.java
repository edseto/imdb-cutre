package cat.dam.edgar.imdb_cutre;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

public class Database
{
    private final CollectionReference usersRef;

    /**
     * Constructor
     */
    public Database()
    {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        usersRef = db.collection(Constants.COLLECTION_USERS);
    }

    /**
     * Insert User
     * @param user ModelUser: User to insert
     */
    public void insertUser(ModelUser user)
    {
        usersRef.document(user.getEmail()).set(user);
    }

    /**
     * Update User
     * @param user ModelUser: User fields to update
     * @param oldEmail String: User email to get Firebase Document
     */
    public void updateUser(ModelUser user, String oldEmail)
    {
        usersRef.document(oldEmail).delete();
        usersRef.document(user.getEmail()).set(user);
    }

    /**
     * Insert Favourite Movie to List
     * @param email String: User email where add movie
     * @param movie ModelFilm: Movie to add in favourite list
     */
    public void insertFavouriteMovies(String email, ModelFilm movie)
    {
        DocumentReference user = usersRef.document(email);
        user.update("movies", FieldValue.arrayUnion(movie.getId()));
    }
}