package cat.dam.edgar.imdb_cutre;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;

/**
 * @author Edgar Seguí Toro
 */

public class MainActivity extends AppCompatActivity
{

    private static final int MY_PERMISSIONS_REQUEST_INTERNET = 1;
    private String language, login, url;
    private int page, totalPages;
    private Boolean clicked = false;
    private RecyclerView rv_main, rv_genres;
    private EditText et_searchBar;
    private ImageButton ib_search;
    private Button btn_filter, btn_next, btn_previous;
    private FloatingActionButton fab_menu, fab_profile;
    private Animation from_bottom, to_bottom;
    private ArrayList<ModelFilm> movieList;
    private ArrayList<ModelGenre> genreList;
    private MainRecyclerAdapter adapter;
    private GenreRecyclerAdapter genreAdapter;
    private Dialog preLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!haveInternet()) internetPermission();

        setVariables();
        setListeners();
        homePage();
    }

    private void internetPermission()
    {
        if(!haveInternet()) ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.INTERNET}, MY_PERMISSIONS_REQUEST_INTERNET);
    }

    private boolean haveInternet()
    {
        return ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        if (requestCode == MY_PERMISSIONS_REQUEST_INTERNET) {
            if (!haveInternet()) Constants.createToast(MainActivity.this, getResources().getString(R.string.internet_error));
        }
    }

    private void setVariables()
    {
        SharedPreferences preferences = getApplicationContext().getSharedPreferences("Preferences", 0);
        login = preferences.getString("LOGIN", null);

        movieList = new ArrayList<>();
        genreList = new ArrayList<>();
        preLoader = new Dialog(this);
        language = Locale.getDefault().toLanguageTag();
        page = 1;
        setAnimationVar();
        setComponents();
    }

    private void setAnimationVar()
    {
        to_bottom = AnimationUtils.loadAnimation(this, R.anim.anim_to_bottom);
        from_bottom = AnimationUtils.loadAnimation(this, R.anim.anim_from_bottom);
    }

    private void setComponents()
    {
        rv_main = (RecyclerView) findViewById(R.id.rv_main);
        rv_genres = (RecyclerView) findViewById(R.id.rv_genres_filter);
        et_searchBar = (EditText) findViewById(R.id.et_searchBar);
        fab_menu = (FloatingActionButton) findViewById(R.id.fab_menu);
        fab_profile = (FloatingActionButton) findViewById(R.id.fab_profile);
        setBtnVar();
    }

    private void setBtnVar()
    {
        ib_search = (ImageButton) findViewById(R.id.ib_search);
        btn_filter = (Button) findViewById(R.id.btn_apply_filter);
        btn_next = (Button) findViewById(R.id.btn_next_page);
        btn_previous = (Button) findViewById(R.id.btn_previous_page);
    }

    private void setListeners()
    {
        searchListener();
        menuListener();
        filterListener();
        pagesListener();
    }

    /**
     * Next and Previous page buttons
     */
    private void pagesListener()
    {
        Resources res = getResources();
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (page < totalPages) {
                    page++;
                    new GetData(url+page).execute();
                } else {
                    Constants.createToast(MainActivity.this, res.getString(R.string.last_page));
                }
            }
        });

        btn_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (page > 1) {
                    page--;
                    new GetData(url+page).execute();
                } else {
                    Constants.createToast(MainActivity.this, res.getString(R.string.first_page));
                }
            }
        });
    }

    /**
     * Genres Filter
     */
    private void filterListener()
    {
        btn_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applyFilter();
            }
        });
    }

    /**
     * Apply Genres Filter
     */
    private void applyFilter()
    {
        String genres = getGenresChecked();
        url = Constants.FILTER_LINK+"?api_key="+Constants.API_KEY+"&language="+language+"&with_genres="+genres+"&page=";

        new GetData(url+page).execute();
    }

    /**
     * Get Checked Genres with pipe bar separator (translated to url code)
     * @return String: genres checked (ex: action%7Cadventure)
     */
    private String getGenresChecked()
    {
        String genres = "";
        int nGenres = genreList.size();

        for (int i = 0; i < nGenres; i++) {
            if (genreList.get(i).isChecked()){
                genres += genreList.get(i).getId();
                if (i < nGenres-1) genres+="%7C"; // %7C == | (pipe bar == "OR")
            }
        }
        
        return genres;
    }

    /**
     * Search Bar Listener
     */
    private void searchListener()
    {
        ib_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search();
            }
        });

        et_searchBar.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH){
                    search();
                    return true;
                }
                return false;
            }
        });
    }

    /**
     * Search Movie
     */
    private void search()
    {
        String search = et_searchBar.getText().toString(), searchURL;
        search = search.replaceAll(" ", "+");

        url = Constants.SEARCH_LINK+"?api_key="+Constants.API_KEY+"&language="+language+"&query="+search+"&page=";

        new GetData(url+page).execute();
    }

    /**
     * Floating Menu
     */
    private void menuListener()
    {
        fab_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onMenuClick();
            }
        });

        fab_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(login == null) goActivity(SignUpUser.class);
                else goActivity(UserProfile.class);
            }
        });
    }

    /**
     * Menu Animations
     */
    private void onMenuClick()
    {
        setVisibility(clicked);
        setAnimation(clicked);
        clicked = !clicked;
    }

    /**
     * Change Menu items visibility
     * @param click Boolean true: open, false: close
     */
    private void setVisibility(Boolean click)
    {
        if(!click){
            fab_profile.setVisibility(View.VISIBLE);
        } else {
            fab_profile.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * Start Menu items Animation
     * @param click Boolean true: open, false: close
     */
    private void setAnimation(Boolean click)
    {
        if(!click){
            fab_profile.startAnimation(from_bottom);
            fab_menu.setImageResource(R.drawable.ic_close);
        } else {
            fab_profile.startAnimation(to_bottom);
            fab_menu.setImageResource(R.drawable.ic_menu);
        }
    }

    /**
     * Other Activities
     * @param activity Activity to go (ex: SignUp.class)
     */
    private void goActivity(Class activity)
    {
        Intent intent = new Intent(this, activity);
        startActivity(intent);
    }

    /**
     * Default HomePage
     */
    private void homePage()
    {
        url = Constants.API_LINK+"popular?api_key="+Constants.API_KEY+"&language="+language+"&page=";

        new GetData(url+page).execute();
        new GetGenreList().execute();
    }

    /**
     * Print Movie RecyclerView Data
     */
    private void printMovieData()
    {
        if (adapter == null) adapter = new MainRecyclerAdapter(this, movieList);
        rv_main.setLayoutManager(new GridLayoutManager(this, 2));
        rv_main.setAdapter(adapter);
    }

    /**
     * Print Genre RecyclerView Data
     */
    private void printGenreFilter()
    {
        if (genreAdapter == null) genreAdapter = new GenreRecyclerAdapter(this, genreList);
        rv_genres.setLayoutManager(new GridLayoutManager(this, 1));
        rv_genres.setAdapter(genreAdapter);
    }

    /**
     * Get URL Data
     */
    public class GetData extends AsyncTask<String, String, String>
    {
        private final String searchURL;

        public GetData(String url) { this.searchURL = url; }

        @Override
        protected void onPreExecute()
        {
            Constants.preLoader(preLoader);
        }

        @Override
        protected String doInBackground(String... strings)
        {
            String actual = "";

            URL url;
            HttpsURLConnection connection = null;

            try {
                url = new URL(searchURL);
                connection = (HttpsURLConnection) url.openConnection();

                InputStream is = connection.getInputStream();
                InputStreamReader isr = new InputStreamReader(is);

                int data = isr.read();
                while (data != -1){
                    actual += (char) data;
                    data = isr.read();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            if(connection != null) connection.disconnect();

            return actual;
        }

        // When finish doInBackground function, goes here
        @Override
        protected void onPostExecute(String s)
        {
            preLoader.dismiss();
            movieList.clear();
            try {
                JSONObject json = new JSONObject(s);
                String pages = json.getString("total_pages");
                totalPages = Integer.parseInt(pages);
                JSONArray jsonArray = json.getJSONArray("results");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonMovie = jsonArray.getJSONObject(i);
                    ModelFilm film = new ModelFilm(jsonMovie, language);
                    movieList.add(film);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            printMovieData();
        }
    }

    /**
     * Get Genre List from URL
     */
    public class GetGenreList extends AsyncTask<String, String, String>
    {
        @Override
        protected void onPreExecute()
        {
            Constants.preLoader(preLoader);
        }

        @Override
        protected String doInBackground(String... strings)
        {
            String searchURL = Constants.GENRE_LIST_LINK+"?api_key="+Constants.API_KEY+"&language="+language;
            String actual = "";

            URL url;
            HttpsURLConnection connection = null;

            try {
                url = new URL(searchURL);
                connection = (HttpsURLConnection) url.openConnection();

                InputStream is = connection.getInputStream();
                InputStreamReader isr = new InputStreamReader(is);

                int data = isr.read();
                while (data != -1){
                    actual += (char) data;
                    data = isr.read();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            if(connection != null) connection.disconnect();

            return actual;
        }

        // When finish doInBackground function, goes here
        @Override
        protected void onPostExecute(String s)
        {
            preLoader.dismiss();
            genreList.clear();
            try {
                JSONObject json = new JSONObject(s);
                JSONArray jsonArray = json.getJSONArray("genres");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonGenre = jsonArray.getJSONObject(i);
                    ModelGenre genre = new ModelGenre(jsonGenre);
                    genreList.add(genre);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            printGenreFilter();
        }
    }
}