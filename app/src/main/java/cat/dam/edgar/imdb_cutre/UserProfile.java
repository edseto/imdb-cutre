package cat.dam.edgar.imdb_cutre;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;

public class UserProfile extends AppCompatActivity
{
    private Boolean clicked = false;
    private String language, newPassword, newEmail, newUsername, currentPassword;
    private TextView tv_username, tv_email;
    private EditText et_username, et_email, et_currentPassword, et_newPassword, et_confirmNewPassword;
    private Button btn_edit, btn_acceptEdit, btn_cancelEdit;
    private ModelUser user;
    private RecyclerView rv_movies;
    private MainRecyclerAdapter adapter;
    private FloatingActionButton fab_menu, fab_logOut, fab_home;
    private Animation from_bottom, to_bottom;
    private ArrayList<ModelFilm> movieList;
    private Dialog editProfile, preLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        setVariables();
        setListeners();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            user = (ModelUser) bundle.getSerializable("user");
            boolean haveMovies = user.getMovies() != null;
            printData(haveMovies);
        } else {
            getUserLogged();
        }
    }

    private void setVariables()
    {
        language = Locale.getDefault().toLanguageTag();
        preLoader = new Dialog(this);
        movieList = new ArrayList<>();
        tv_username = (TextView) findViewById(R.id.tv_profile_username);
        tv_email = (TextView) findViewById(R.id.tv_profile_email);
        rv_movies = (RecyclerView) findViewById(R.id.rv_favMovies);
        btn_edit = (Button) findViewById(R.id.btn_edit_profile);
        editProfile = new Dialog(this);
        preLoader = new Dialog(this);
        setFabVar();
        setAnimationVar();
    }

    private void setFabVar()
    {
        fab_menu = (FloatingActionButton) findViewById(R.id.fab_profile_menu);
        fab_home = (FloatingActionButton) findViewById(R.id.fab_profile_home);
        fab_logOut = (FloatingActionButton) findViewById(R.id.fab_profile_logOut);
    }

    private void setAnimationVar()
    {
        to_bottom = AnimationUtils.loadAnimation(this, R.anim.anim_to_bottom);
        from_bottom = AnimationUtils.loadAnimation(this, R.anim.anim_from_bottom);
    }

    private void setListeners()
    {
        fab_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onMenuClick();
            }
        });

        fab_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goHomePage();
            }
        });

        fab_logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logOut();
            }
        });

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editProfile();
            }
        });
    }

    /**
     * Show Edit Profile PopUp
     */
    private void editProfile()
    {
        editProfile.setContentView(R.layout.edit_profile);
        setEditVar();
        setEditListener();
        editProfile.show();
    }

    private void setEditVar()
    {
        et_username = (EditText) editProfile.findViewById(R.id.et_edit_username);
        et_email = (EditText) editProfile.findViewById(R.id.et_edit_email);
        et_currentPassword = (EditText) editProfile.findViewById(R.id.et_currentPassword);
        et_newPassword = (EditText) editProfile.findViewById(R.id.et_new_password);
        et_confirmNewPassword = (EditText) editProfile.findViewById(R.id.et_confirm_new_password);
        btn_acceptEdit = (Button) editProfile.findViewById(R.id.btn_edit_accept);
        btn_cancelEdit = (Button) editProfile.findViewById(R.id.btn_edit_cancel);
    }

    private void setEditListener()
    {
        btn_cancelEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editProfile.dismiss();
            }
        });

        btn_acceptEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkEditFields()) editFields();
            }
        });
    }

    /**
     * Save non empty fields
     * @return if current password is correct
     */
    private boolean checkEditFields()
    {
        Resources res = getResources();

        if (TextUtils.isEmpty(et_currentPassword.getText())){
            et_currentPassword.setError(res.getString(R.string.current_password_error));
            return false;
        } else {
            try {
                currentPassword = Constants.encryptPassword(et_currentPassword.getText().toString());
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }

        if (!TextUtils.isEmpty(et_email.getText())) newEmail = et_email.getText().toString();
        if (!TextUtils.isEmpty(et_username.getText())) newUsername = et_username.getText().toString();

        return checkNewPassword();
    }

    /**
     * Check if new passwords match
     * @return if passwords match save and return true else return false
     */
    private boolean checkNewPassword()
    {
        Resources res = getResources();
        String newPwd = et_newPassword.getText().toString(), confirmPwd = et_confirmNewPassword.getText().toString();

        if (!TextUtils.isEmpty(et_newPassword.getText())){
            if (newPwd.equals(confirmPwd)){
                try {
                    newPassword = Constants.encryptPassword(et_newPassword.getText().toString());
                    return true;
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
            } else {
                et_confirmNewPassword.setError(res.getString(R.string.password_no_match));
                return false;
            }
        }

        return true; // return true because it's optional
    }

    /**
     * Update User fields
     */
    private void editFields()
    {
        if (checkCurrentPassword()){
            Database db = new Database();
            String oldEmail = user.getEmail();

            if (!TextUtils.isEmpty(newEmail)) user.setEmail(newEmail);
            if (!TextUtils.isEmpty(newUsername)) user.setUsername(newUsername);
            if (!TextUtils.isEmpty(newPassword)) {
                try {
                    user.setPassword(newPassword, false);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
            }

            db.updateUser(user, oldEmail);
            printData(false);
            editProfile.dismiss();
        } else {
            Constants.createToast(this, getResources().getString(R.string.password_incorrect));
        }
    }

    /**
     *
     * @return if current password is correct
     */
    private boolean checkCurrentPassword()
    {
        return user.getPassword().equals(currentPassword);
    }

    /**
     * Menu Animations
     */
    private void onMenuClick()
    {
        setVisibility(clicked);
        setAnimation(clicked);
        clicked = !clicked;
    }

    /**
     * Change Menu items visibility
     * @param click Boolean true: open, false: close
     */
    private void setVisibility(Boolean click)
    {
        if(!click){
            fab_home.setVisibility(View.VISIBLE);
            fab_logOut.setVisibility(View.VISIBLE);
        } else {
            fab_home.setVisibility(View.INVISIBLE);
            fab_logOut.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * Start Menu items Animation
     * @param click Boolean true: open, false: close
     */
    private void setAnimation(Boolean click)
    {
        if(!click){
            fab_home.startAnimation(from_bottom);
            fab_logOut.startAnimation(from_bottom);
            fab_menu.setImageResource(R.drawable.ic_close);
        } else {
            fab_home.startAnimation(to_bottom);
            fab_logOut.startAnimation(to_bottom);
            fab_menu.setImageResource(R.drawable.ic_menu);
        }
    }

    /**
     * Print User data
     * @param haveMovies true if there are favourite movies
     */
    private void printData(boolean haveMovies)
    {
        tv_username.setText(user.getUsername());
        tv_email.setText(user.getEmail());
        if (haveMovies) printMovieData();
    }

    /**
     * Print favourite movies list
     */
    private void printMovieData()
    {
        if (adapter == null) adapter = new MainRecyclerAdapter(this, movieList);
        rv_movies.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rv_movies.setAdapter(adapter);
    }

    /**
     * Get User Logged in preferences
     */
    private void getUserLogged()
    {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        SharedPreferences preferences = getSharedPreferences("LOGIN", 0);
        String email = preferences.getString("LOGIN", null);
        user = new ModelUser();

        db.collection(Constants.COLLECTION_USERS).document(email).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()){
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        user.setEmail((String) document.get("email"));
                        user.setUsername((String) document.get("username"));
                        try {
                            user.setPassword((String) document.get("password"), false);
                        } catch (NoSuchAlgorithmException e) {
                            e.printStackTrace();
                        }
                        ArrayList<String> movies = (ArrayList<String>) document.get("movies");
                        if (movies != null) getFavMovies(movies);
                        else printData(false);
                    }
                }
            }
        });
    }

    /**
     * Log Out and Delete session from preferences
     */
    private void logOut()
    {
        SharedPreferences preferences = getApplicationContext().getSharedPreferences("Preferences", 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove("LOGIN");
        editor.apply();

        goHomePage();
    }

    /**
     * Go MainActivity
     */
    private void goHomePage()
    {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    /**
     * Get favourite Movies List from Firebase
     * @param movies Firebase ArrayList of movies id
     */
    private void getFavMovies(ArrayList<String> movies)
    {
        String url;
        int total = movies.size();
        for (String idMovie: movies) {
            url = Constants.API_LINK+idMovie+"?api_key="+Constants.API_KEY+"&language="+language;
            new GetMovie(url, total).execute();
        }
    }

    /**
     * Get Favourite Movie from id
     */
    public class GetMovie extends AsyncTask<String, String, String>
    {
        private final String searchURL;
        private final int total;

        public GetMovie(String movie, int total)
        {
            this.searchURL = movie;
            this.total = total;
        }

        @Override
        protected void onPreExecute()
        {
            Constants.preLoader(preLoader);
        }

        @Override
        protected String doInBackground(String... strings)
        {
            String actual = "";

            URL url;
            HttpsURLConnection connection = null;

            try {
                url = new URL(searchURL);
                connection = (HttpsURLConnection) url.openConnection();

                InputStream is = connection.getInputStream();
                InputStreamReader isr = new InputStreamReader(is);

                int data = isr.read();
                while (data != -1){
                    actual += (char) data;
                    data = isr.read();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            if(connection != null) connection.disconnect();

            return actual;
        }

        // When finish doInBackground function, goes here
        @Override
        protected void onPostExecute(String s)
        {
            preLoader.dismiss();
            try {
                JSONObject jsonMovie = new JSONObject(s);
                ModelFilm film = new ModelFilm(jsonMovie, language);
                movieList.add(film);
                if (total == movieList.size()){
                    user.setFavouritesMovie(movieList);
                    printData(true);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }
}