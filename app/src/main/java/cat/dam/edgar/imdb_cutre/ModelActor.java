package cat.dam.edgar.imdb_cutre;

import org.json.JSONException;
import org.json.JSONObject;

public class ModelActor
{
    private String name, character, image;

    public String getName() { return name; }
    public String getCharacter() { return character; }
    public String getImage() { return image; }

    public void setName(String name) { this.name = name; }
    public void setCharacter(String character) { this.character = character; }
    public void setImage(String image) { this.image = image; }

    public ModelActor()
    {}

    public ModelActor(String name, String character, String image)
    {
        this.name = name;
        this.character = character;
        this.image = image;
    }

    public ModelActor(JSONObject jsonActor) throws JSONException
    {
        setName(jsonActor.getString("name"));
        setCharacter(jsonActor.getString("character"));
        String img = Constants.IMAGE_LINK + jsonActor.getString("profile_path");
        setImage(img);
    }

}