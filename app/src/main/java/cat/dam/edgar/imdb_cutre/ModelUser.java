package cat.dam.edgar.imdb_cutre;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

public class ModelUser implements Serializable
{
    private String username, email, password;
    private ArrayList<ModelFilm> movies;

    // Getters
    public String getUsername() { return username; }
    public String getEmail() { return email; }
    public String getPassword() { return password; }
    public ArrayList<ModelFilm> getMovies() { return movies; }

    // Setters
    public void setUsername(String username) { this.username = username; }
    public void setEmail(String email) { this.email = email; }
    public void setFavouritesMovie(ArrayList<ModelFilm> movies) { this.movies = movies; }
    public void setPassword(String password, boolean encrypt) throws NoSuchAlgorithmException
    {
        if (encrypt) this.password = Constants.encryptPassword(password);
        else this.password = password;
    }

    // Constructors
    public ModelUser()
    {
        movies = new ArrayList<>();
    }

    public ModelUser(String username, String email, String password) throws NoSuchAlgorithmException
    {
        this.username = username;
        this.email = email;
        setPassword(password, true);
    }
}