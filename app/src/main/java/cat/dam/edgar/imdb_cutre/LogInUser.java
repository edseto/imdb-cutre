package cat.dam.edgar.imdb_cutre;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.security.NoSuchAlgorithmException;
import java.util.Objects;

public class LogInUser extends AppCompatActivity
{
    private EditText et_email, et_password;
    private Button btn_logIn, btn_signUp;
    private FloatingActionButton fab_home;
    private final ModelUser user = new ModelUser();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in_user);

        setVariables();
        setListeners();
    }

    private void setVariables()
    {
        et_email = (EditText) findViewById(R.id.et_logIn_email);
        et_password = (EditText) findViewById(R.id.et_logIn_password);
        btn_logIn = (Button) findViewById(R.id.btn_logIn);
        btn_signUp = (Button) findViewById(R.id.btn_logIn_to_signUp);
        fab_home = (FloatingActionButton) findViewById(R.id.fab_logIn_home);
    }

    private void setListeners()
    {
        btn_logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Resources res = getResources();
                if (checkFields()) checkLogIn(et_email.getText().toString());
                else Constants.createToast(getApplicationContext(), res.getString(R.string.logIn_error));
            }
        });

        fab_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goActivity(MainActivity.class);
            }
        });

        btn_signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goActivity(SignUpUser.class);
            }
        });
    }

    /**
     * Check if fields are empty
     * @return Boolean: true if correct and false if incorrect
     */
    private boolean checkFields()
    {
        Resources res = getResources();

        if (TextUtils.isEmpty(et_email.getText())){
            et_email.setError(res.getString(R.string.email_error));
            return false;
        }

        if (TextUtils.isEmpty(et_password.getText())){
            et_password.setError(res.getString(R.string.password_error));
            return false;
        }

        return true;
    }

    /**
     * Check if fields are correct in Firebase
     * @param email String: User email to log in
     */
    private void checkLogIn(String email)
    {
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection(Constants.COLLECTION_USERS).document(email).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                Resources res = getResources();
                if (task.isSuccessful()){
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) checkPassword(document);
                    else Constants.createToast(LogInUser.this, res.getString(R.string.user_no_exists));

                } else {
                    Constants.createToast(LogInUser.this, res.getString(R.string.logIn_error));
                }
            }
        });
    }

    /**
     * Check if password is correct
     * @param document DocumentSnapshot: Firebase user document
     */
    private void checkPassword(DocumentSnapshot document)
    {
        Resources res = getResources();
        String dbPassword = document.get("password").toString(), userPassword;

        try {
            userPassword = Constants.encryptPassword(et_password.getText().toString());
            if(!userPassword.equals(dbPassword)) Constants.createToast(LogInUser.this, res.getString(R.string.logIn_error));
            else logInCorrect(document);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    /**
     * User Log in
     * @param document DocumentSnapshot: User Firebase document to log in
     * @throws NoSuchAlgorithmException
     */
    private void logInCorrect(DocumentSnapshot document) throws NoSuchAlgorithmException
    {
        user.setUsername(Objects.requireNonNull(document.get("username")).toString());
        user.setEmail(Objects.requireNonNull(document.get("email")).toString());
        user.setPassword(Objects.requireNonNull(document.get("password")).toString(), false);

        goProfile();
    }

    /**
     * Other Activities
     * @param activity Activity to go (ex: SignUp.class)
     */
    private void goActivity(Class activity)
    {
        Intent intent = new Intent(this, activity);
        startActivity(intent);
    }

    /**
     * Go to Profile Activity
     */
    private void goProfile()
    {
        saveLogIn();
        Intent intent = new Intent(this, UserProfile.class);
        Bundle bundle = new Bundle();

        bundle.putSerializable("user", user);
        intent.putExtras(bundle);

        startActivity(intent);
    }

    /**
     * Save User Log in in preferences
     */
    private void saveLogIn()
    {
        SharedPreferences preferences = getApplicationContext().getSharedPreferences("Preferences", 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("LOGIN", user.getEmail());
        editor.apply();
    }
}